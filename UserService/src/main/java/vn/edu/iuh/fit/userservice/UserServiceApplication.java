package vn.edu.iuh.fit.userservice;

import net.datafaker.Faker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import vn.edu.iuh.fit.userservice.models.User;
import vn.edu.iuh.fit.userservice.repositories.UserRepository;

@SpringBootApplication
public class UserServiceApplication {

    public UserServiceApplication(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }

//    @Autowired
    private final UserRepository userRepository;

    private final Faker faker = new Faker();

//    @Bean
//    CommandLineRunner commandLineRunner(){
//        return new CommandLineRunner() {
//            @Override
//            public void run(String... args) throws Exception {
//                for (int i=1; i<=10; i++){
//                    userRepository.save(new User(i, "User" + i));
//                }
//            }
//        };
//    }

    @Bean
    public CommandLineRunner commandLineRunner(){
        return args -> {
            for(int i = 1; i <= 10; i++){
                User user = createFakerUser();
                userRepository.save(user);
            }
        };
    }

    private User createFakerUser(){
        User user = new User();
        user.setName(faker.name().fullName());
        user.setEmail(faker.internet().emailAddress());
        user.setPhone(faker.phoneNumber().phoneNumber());
        return user;
    }

}
