package vn.edu.iuh.fit.supplierservice;

import net.datafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import vn.edu.iuh.fit.supplierservice.models.Supplier;
import vn.edu.iuh.fit.supplierservice.repositories.SupplierRepository;

@SpringBootApplication
public class SupplierServiceApplication {
    private final SupplierRepository supplierRepository;
    private final Faker faker = new Faker();

    public SupplierServiceApplication(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(SupplierServiceApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(){
        return args -> {
            for (int i = 1; i <= 10; i++){
                Supplier supplier =createFakerSupplier();
                supplierRepository.save(supplier);
            }
        };
    }

    private Supplier createFakerSupplier() {
        Supplier supplier = new Supplier();
        supplier.setName(faker.company().name());
        supplier.setAddress(faker.address().fullAddress());
        supplier.setPhone(faker.phoneNumber().phoneNumber());
        return supplier;
    }

}
