package vn.edu.iuh.fit.supplierservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.edu.iuh.fit.supplierservice.models.Supplier;
import vn.edu.iuh.fit.supplierservice.services.SupplierService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    public SupplierController(SupplierService supplierService) {
        this.supplierService = supplierService;
    }

    @GetMapping("/suppliers")
    public List<Supplier> getListSupplier(){
        return supplierService.getListSupplier();
    }

    @GetMapping("/suppliers/{id}")
    public Supplier getSupplierById(@PathVariable long id){
        return supplierService.getSupplierById(id);
    }
}
