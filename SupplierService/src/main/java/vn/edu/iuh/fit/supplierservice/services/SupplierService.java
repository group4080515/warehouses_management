package vn.edu.iuh.fit.supplierservice.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.edu.iuh.fit.supplierservice.models.Supplier;
import vn.edu.iuh.fit.supplierservice.repositories.SupplierRepository;

import java.util.List;

@Slf4j
@Service
public class SupplierService {
    private final SupplierRepository supplierRepository ;


    public SupplierService(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    public List<Supplier> getListSupplier(){
        return supplierRepository.findAll();
    }

    public Supplier getSupplierById(long id){
        return supplierRepository.findById(id).get();
    }
}
