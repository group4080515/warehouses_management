package vn.edu.iuh.fit.productservice.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Entity
@Table(name = "inventorys")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Inventory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "inventory_id")
    private long id;
    @Column(name = "inventory_quantity")
    private double inventory_quantity;

}
