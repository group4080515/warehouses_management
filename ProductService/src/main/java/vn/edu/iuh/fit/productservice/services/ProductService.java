package vn.edu.iuh.fit.productservice.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.productservice.models.Inventory;
import vn.edu.iuh.fit.productservice.models.Product;
import vn.edu.iuh.fit.productservice.repositories.ProductRepository;

import java.util.List;

@Slf4j
@Service
public class ProductService {
    private final ProductRepository productRepository;

    private final RestTemplate restTemplate;

    public ProductService(ProductRepository productRepository, RestTemplate restTemplate) {
        this.productRepository = productRepository;
        this.restTemplate = restTemplate;
    }

    public List<Product> getAllProduct(){
        List<Product> productList = productRepository.findAll();
        for (Product p: productList){
            Inventory inventory = restTemplate.getForObject("http://localhost:8084/inventory/"+p.getId(), Inventory.class);
            p.setInventory(inventory);
        }
        return productList;
    }

    public Product getProductById(long id){
        Product product = productRepository.findById(id).get();
        Inventory inventory = restTemplate.getForObject("http://localhost:8084/inventory/"+id, Inventory.class);
        product.setInventory(inventory);
        return product;
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public void deleteProductById(long id){
        productRepository.deleteById(id);
    }
}
