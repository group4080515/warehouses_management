package vn.edu.iuh.fit.productservice.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import vn.edu.iuh.fit.productservice.models.Product;
import vn.edu.iuh.fit.productservice.services.ProductService;

import java.util.List;

@RestController()
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductService productService;
    @GetMapping("/products")
    @Cacheable(value = "products")
    public List<Product> getAllProduct(){
        return productService.getAllProduct();
    }

    @GetMapping("/products/{id}")
    @Cacheable(value = "product", key = "#id")
    public Product getProductById(@PathVariable long id){
        return productService.getProductById(id);
    }

    @PostMapping("/products")
    @Cacheable(value = "product", key = "#result.id")
    public Product addProduct(@RequestBody Product product){
        return productService.addProduct(product);
    }

    @DeleteMapping("/products/{productId}")
    @Cacheable(value = "product")
    public void deleteProduct(@PathVariable(value = "productId") long productId){
        productService.deleteProductById(productId);
    }
}
