package vn.edu.iuh.fit.auth_jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.edu.iuh.fit.auth_jwt.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
