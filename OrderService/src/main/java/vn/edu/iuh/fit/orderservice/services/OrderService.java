package vn.edu.iuh.fit.orderservice.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import vn.edu.iuh.fit.orderservice.dto.InventoryResponse;
import vn.edu.iuh.fit.orderservice.dto.OderLineItemsDto;
import vn.edu.iuh.fit.orderservice.dto.OrderRequest;
import vn.edu.iuh.fit.orderservice.models.Order;
import vn.edu.iuh.fit.orderservice.models.OrderLineItems;
import vn.edu.iuh.fit.orderservice.repositories.OrderRepository;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderService {
    private final OrderRepository orderRepository;
    private final WebClient webClient;

   public String placeOrder(OrderRequest orderRequest) {
      Order order = new Order();
      order.setOrderNumber(UUID.randomUUID().toString());

       List<OrderLineItems> orderLineItems = orderRequest.getOrderLineItemsDtoList().stream()
               .map(this::mapToDto).toList();

       order.setOrderLineItemsList(orderLineItems);

       List<String> skuCodes = order.getOrderLineItemsList().stream()
               .map(OrderLineItems::getSkuCode)
               .toList();

       //Call inventory service
       //stock
       InventoryResponse[] inventoryResponsArray = webClient.get().uri("http://localhost:8084/api/inventory",
               uriBuilder -> uriBuilder.queryParam("skuCode", skuCodes).build()
               ).retrieve().bodyToMono(InventoryResponse[].class).block();

       boolean allProductsInStock = Arrays.stream(inventoryResponsArray).allMatch(inventoryResponse -> inventoryResponse.isInStock());

       if(allProductsInStock) {
           orderRepository.save(order);
           return "Order placed successfully";
       } else {
           throw new IllegalArgumentException("Out of stock");
       }

   }

    private OrderLineItems mapToDto(OderLineItemsDto orderLineItemsDto) {
        OrderLineItems orderLineItems = new OrderLineItems();
        orderLineItems.setPrice(orderLineItemsDto.getPrice());
        orderLineItems.setQuantity(orderLineItemsDto.getQuantity());
        orderLineItems.setSkuCode(orderLineItemsDto.getSkuCode());
        return orderLineItems;
    }

}
