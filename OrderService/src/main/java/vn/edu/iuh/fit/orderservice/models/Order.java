package vn.edu.iuh.fit.orderservice.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "t_orders")
@AllArgsConstructor @NoArgsConstructor
@Getter
@Setter
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
   private String orderNumber;
   @OneToMany(cascade = CascadeType.ALL)
   private List<OrderLineItems> orderLineItemsList;
}
