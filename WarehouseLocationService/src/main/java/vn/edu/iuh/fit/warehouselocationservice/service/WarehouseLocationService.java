package vn.edu.iuh.fit.warehouselocationservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.warehouselocationservice.models.Inventory;
import vn.edu.iuh.fit.warehouselocationservice.models.WarehouseLocation;
import vn.edu.iuh.fit.warehouselocationservice.repositories.WarehouseLocationRepository;

import java.util.List;

@Slf4j
@Service
public class WarehouseLocationService {
    private final WarehouseLocationRepository warehouseLocationRepository;
    private final RestTemplate restTemplate;


    public WarehouseLocationService(WarehouseLocationRepository warehouseLocationRepository, RestTemplate restTemplate) {
        this.warehouseLocationRepository = warehouseLocationRepository;
        this.restTemplate = restTemplate;
    }

    public List<WarehouseLocation> getAllLocation(){
        List<WarehouseLocation> locations = warehouseLocationRepository.findAll();
        for (WarehouseLocation w : locations){
            Inventory inventory = restTemplate.getForObject("http://localhost:8084/inventorys/"+w.getId(), Inventory.class);
            w.setInventory(inventory);
        }
        return locations;
    }

    public WarehouseLocation getLocationById(long id){
        WarehouseLocation location = warehouseLocationRepository.findById(id).get();
        Inventory inventory = restTemplate.getForObject("http://localhost:8084/inventorys/"+id, Inventory.class);
        location.setInventory(inventory);
        return location;
    }

    public WarehouseLocation addLocation(WarehouseLocation location){
        return warehouseLocationRepository.save(location);
    }

    public void deleteLocationById(long id){
        warehouseLocationRepository.deleteById(id);
    }
}
