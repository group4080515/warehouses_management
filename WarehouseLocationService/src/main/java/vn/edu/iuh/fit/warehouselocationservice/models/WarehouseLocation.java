package vn.edu.iuh.fit.warehouselocationservice.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "warehouseLocation")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WarehouseLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "warehouse_location_id")
    private long id;
    @Column(name = "location_name")
    private String locationName;
    @Column(name = "location_des")
    private String locationDescription;
    @Column(name = "location_type")
    private String locationType;
    @Column(name = "capacity")
    private Integer capacity;
    @Column(name = "status")
    private String status;
    @Column(name = "last_update")
    private LocalDateTime lastUpdated;
    @OneToOne
    private Inventory inventory;

    public WarehouseLocation(long id, String locationName, String locationDescription, String locationType, Integer capacity, String status, LocalDateTime lastUpdated) {
        this.id = id;
        this.locationName = locationName;
        this.locationDescription = locationDescription;
        this.locationType = locationType;
        this.capacity = capacity;
        this.status = status;
        this.lastUpdated = lastUpdated;
    }
}
