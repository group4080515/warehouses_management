package vn.edu.iuh.fit.warehouselocationservice;

import net.datafaker.Faker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import vn.edu.iuh.fit.warehouselocationservice.models.WarehouseLocation;
import vn.edu.iuh.fit.warehouselocationservice.repositories.WarehouseLocationRepository;

import java.time.LocalDateTime;

@SpringBootApplication
public class WarehouseLocationServiceApplication {

    private final WarehouseLocationRepository locationRepository;

    private final Faker faker = new Faker();

    public WarehouseLocationServiceApplication(WarehouseLocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(WarehouseLocationServiceApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(){
        return args -> {
            for (int i = 1; i <= 10; i++){
                WarehouseLocation location = createFakerLocation();
                locationRepository.save(location);
            }
        };
    }

    private WarehouseLocation createFakerLocation() {
        WarehouseLocation location = new WarehouseLocation();
        location.setLocationName(faker.address().buildingNumber());
        location.setLocationDescription(faker.lorem().sentence());
        location.setLocationType(faker.options().option("Ke", "Ngan", "Khu vuc"));
        location.setCapacity(faker.number().numberBetween(1, 1000));
        location.setStatus(faker.options().option("Trong", "Day", "Bao tr�"));
        location.setLastUpdated(LocalDateTime.now());
        return location;
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

}
