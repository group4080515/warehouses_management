package vn.edu.iuh.fit.warehouselocationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import vn.edu.iuh.fit.warehouselocationservice.models.WarehouseLocation;
import vn.edu.iuh.fit.warehouselocationservice.service.WarehouseLocationService;

import java.util.List;

@RestController()
@RequestMapping("/api")
public class warehouseLocationController {
    @Autowired
    private WarehouseLocationService locationService;
    @GetMapping("/locations")
    @Cacheable(value = "locations")
    public List<WarehouseLocation> getAllLocation(){
        return locationService.getAllLocation();
    }

    @GetMapping("/locatoions/{id}")
    @Cacheable(value = "locatoion", key = "#id")
    public WarehouseLocation getLocationId(@PathVariable long id){
        return locationService.getLocationById(id);
    }

    @PostMapping("/locatoions")
    @Cacheable(value = "locatoion", key = "#result.id")
    public WarehouseLocation addLocation(@RequestBody WarehouseLocation location){
        return locationService.addLocation(location);
    }

    @DeleteMapping("/locations/{locationId}")
    @Cacheable(value = "locatoion")
    public void deleteLocation(@PathVariable(value = "locationId") long locationId){
        locationService.deleteLocationById(locationId);
    }
}
