package vn.edu.iuh.fit.warehouselocationservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.edu.iuh.fit.warehouselocationservice.models.WarehouseLocation;
@Repository
public interface WarehouseLocationRepository extends JpaRepository<WarehouseLocation, Long> {
}
